(function ($, Drupal) {
  Drupal.behaviors.acceptClick = {
    attach: function (context, settings) {
      $(document).ready(function () {
         if(!document.cookie.includes('agegate')){
          $('.age-gate').css('display', 'flex');
         }
      })
      $('.age-gate-button-accept', context).on('click', function () {
        document.cookie = "agegate=1";
        $('.age-gate').fadeOut(250)
      });
      $('.age-gate-button-decline', context).on('click', function () {
        if(document.documentElement.lang == 'en'){
          var html = '<p>Thank you for your interest in Hochriegl Sekt.<br>Our Website is only accessible if your are 18 or older.</p>'
        } else {
          var html = '<p>Vielen Dank für Ihr Interesse in Hochriegl Sekt.<br>Unsere Website kann jedoch erst ab einem Mindestalter von 18 Jahren besucht werden.</p>'
        }
        $('.age-gate-buttons').removeClass('d-flex').addClass('d-none');
        var element = $(this).parent().parent().find('.age-gate-text');
        element.children('p').first().html(html);
      });
    }
  };
})(jQuery, Drupal);
