<?php

namespace Drupal\agegate_popup\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * Provides a 'AgeGateBlock' block.
 *
 * @Block(
 *  id = "age_gate_block",
 *  admin_label = @Translation("Age gate block"),
 * )
 */
class AgeGateBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['field_bg_image'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Background Image'),
      '#default_value' => $this->configuration['field_image'],
      '#description'          => t('Allowed extensions: gif png jpg jpeg'),
      '#upload_location' => 'public://agegate/',
      '#upload_validators'    => [
        'file_validate_extensions'    => array('gif png jpg jpeg'),
      ],
      '#weight' => '0',
    ];
    $form['field_image'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Image'),
      '#default_value' => $this->configuration['field_image'],
      '#description'          => t('Allowed extensions: gif png jpg jpeg svg'),
      '#upload_location' => 'public://agegate/',
      '#upload_validators'    => [
        'file_validate_extensions'    => array('gif png jpg jpeg svg'),
      ],
      '#weight' => '0',
    ];
    $form['field_text'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Text'),
      '#default_value' => $this->configuration['field_text'],
      '#weight' => '0',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $image = $form_state->getValue('field_image');
    if ($image != $this->configuration['field_image']) {
      if (!empty($image[0])) {
        $file = File::load($image[0]);
        $file->setPermanent();
        $file->save();
      }
    }
    $bg_image = $form_state->getValue('field_bg_image');
    if ($bg_image != $this->configuration['field_bg_image']) {
      if (!empty($bg_image[0])) {
        $file = File::load($bg_image[0]);
        $file->setPermanent();
        $file->save();
      }
    }

    $this->configuration['field_bg_image'] = $form_state->getValue('field_bg_image');
    $this->configuration['field_image'] = $form_state->getValue('field_image');
    $this->configuration['field_text'] = $form_state->getValue('field_text')['value'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // if (\Drupal::request()->getHost() === 'hochriegl.dev.eyedea.at') {
    //   return;
    // }

      $build = [];
      $build['#theme'] = 'age_gate_block';
      $image = $this->configuration['field_image'];
      if (!empty($image[0])) {
        if ($file = File::load($image[0])) {
          $build['#content']['field_image'] = [
            '#theme' => 'image',
            '#uri' => $file->getFileUri(),
          ];
        }
      }
      $bg_image = $this->configuration['field_bg_image'];
      if (!empty($bg_image[0])) {
        if ($file = File::load($bg_image[0])) {
          $build['#content']['field_bg_image'] = [
            '#theme' => 'image',
            '#uri' => $file->getFileUri(),
          ];
        }
      }
      $build['#content']['field_text'] = $this->configuration['field_text'];
      $build['#cache'] = [
        'max-age' => -1,
        'contexts' => [
          'cookies:agegate'
        ]
      ];
      $build['#attached']['library'][] = 'agegate_popup/agegate';
      return $build;
  }
}
