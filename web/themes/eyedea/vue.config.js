module.exports = {
  runtimeCompiler: true,
  publicPath: process.env.VUE_APP_THEME_PATH,
  productionSourceMap: false,
  filenameHashing: false,
  css: {
    loaderOptions: {
      sass: {
        prependData: '@import "~@/scss/bootstrap/custom";'
      }
    }
  },
  configureWebpack: {
    optimization: {
      splitChunks: false
    },
    output: {
      filename: 'app.js'
    }
  },
  devServer: {
    inline: false,
    disableHostCheck: true,
    writeToDisk: true
  },
  // delete HTML related webpack plugins
  chainWebpack: (config) => {
    config.plugins.delete('html')
    config.plugins.delete('preload')
    config.plugins.delete('prefetch')

    config.resolve.symlinks(false)

    config.module
      .rule('vue')
      .use('vue-loader')
      .loader('vue-loader')
      .tap((options) => {
        options.compilerOptions.whitespace = 'condense'
        options.transformAssetUrls = { AppImage: 'src' }

        return options
      })

    const svgRule = config.module.rule('svg')
    svgRule.uses.clear()
    svgRule
      .oneOf('external')
      .resourceQuery(/external/)
      .use('file-loader')
      .loader('file-loader')
      .options({
        name: 'assets/[name].[hash:8].[ext]'
      })
      .end()
      .end()
      .oneOf('inline')
      .use('babel-loader')
      .loader('babel-loader')
      .end()
      .use('vue-svg-loader')
      .loader('vue-svg-loader')
      .end()
      .end()
  },

  transpileDependencies: [
    'axios'
  ]
}
