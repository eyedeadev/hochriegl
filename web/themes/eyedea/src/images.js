/* eslint-disable */
export { default as GradientBg } from '@/assets/img/gradient-bg.jpg'
export { default as GradientMobileBg } from '@/assets/img/gradient-bg-mobile.jpg'
export { default as Logo } from '@/assets/svg/logo-colour.svg?external'
export { default as LogoWhite } from '@/assets/svg/logo-white.svg?external'
