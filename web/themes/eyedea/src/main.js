import 'core-js/stable'
import 'regenerator-runtime/runtime'
import Vue from 'vue'

import store from '@/store'
import '@/App.vue'

import AppImage from '@/components/global/AppImage.vue'
import AppIcon from '@/components/global/AppIcon.vue'
import CrosssellingMobile from '@/components/global/CrosssellingMobile.vue'
import HeaderNav from '@/components/global/HeaderNav.vue'
import GallerySlider from '@/components/global/GallerySlider.vue'
import ProductSlider from '@/components/global/ProductSlider.vue'

Vue.component(AppImage.name, AppImage)
Vue.component(AppIcon.name, AppIcon)
Vue.component(CrosssellingMobile.name, CrosssellingMobile)
Vue.component(HeaderNav.name, HeaderNav)
Vue.component(GallerySlider.name, GallerySlider)
Vue.component(ProductSlider.name, ProductSlider)

Vue.config.productionTip = false

new Vue({
  name: 'App',
  store,
  comments: process.env.NODE_ENV !== 'production'
}).$mount('#app')
